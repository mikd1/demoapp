using Microsoft.AspNetCore.Mvc;

namespace Demo.ServiceB.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AboutController : ControllerBase
    {
        [HttpGet]
        public ActionResult<string> Get()
        {
            return "Hello from Service B.";
        }
    }
}

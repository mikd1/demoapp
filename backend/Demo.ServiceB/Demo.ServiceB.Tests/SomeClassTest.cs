using Demo.ServiceB.WebApi;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Demo.ServiceB.Tests
{
    [TestClass]
    public class SomeClassTest
    {
        [TestMethod]
        public void SumIsCorrect()
        {
            SomeClass someClass = new SomeClass();
            Assert.AreEqual(5, someClass.Sum(3, 2));
        }
    }
}
